//
//  ModificationVC.m
//  XQContact
//
//  Created by ladystyle100 on 2017/8/29.
//  Copyright © 2017年 SyKingW. All rights reserved.
//

#import "ModificationVC.h"
#import "XQABManager.h"
#import "ModificationTableViewCell.h"
#import "MDetailVC.h"

static NSString *reusing_ = @"MVCCell";

@interface ModificationVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray <XQABModel *> *dataArr;

@end

@implementation ModificationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.tableView];
}

- (void)getData {
    __weak typeof(self) weakSelf = self;
    [[XQABManager manager] getAllContactWithCompletion:^(NSArray<XQABModel *> *modelArr) {
        weakSelf.dataArr = [modelArr mutableCopy];
        [weakSelf.tableView reloadData];
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getData];
}

#pragma mark -- UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ModificationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reusing_ forIndexPath:indexPath];
    
    XQABModel *model = self.dataArr[indexPath.row];
    
    cell.contactID.text = [NSString stringWithFormat:@"id %d", model.contactID];
    cell.nameLabel.text = [NSString stringWithFormat:@"name %@%@", model.lastName, model.firstName];
    cell.organizationLabel.text = [NSString stringWithFormat:@"organization %@", model.organization];
    cell.phoneLabel.text = [NSString stringWithFormat:@"phoneLabel %@", model.phoneLabelArr.firstObject];
    cell.phone.text = [NSString stringWithFormat:@"phone %@", model.phoneArr.firstObject];
    
    cell.imgView.image = [UIImage imageWithData:model.imgData];
    
    return cell;
}

#pragma mark -- UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MDetailVC *vc = [[MDetailVC alloc] initWithNibName:NSStringFromClass([MDetailVC class]) bundle:nil];
    vc.model = self.dataArr[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark -- get

- (UITableView *)tableView {
    
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        UINib *nib = [UINib nibWithNibName:NSStringFromClass([ModificationTableViewCell class]) bundle:nil];
        [_tableView registerNib:nib forCellReuseIdentifier:reusing_];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.rowHeight = 160;
        
    }
    return _tableView;
}


@end


















