//
//  XQABModel.h
//  SmartCommunity
//
//  Created by ladystyle100 on 2017/5/11.
//  Copyright © 2017年 Ladystyle100. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XQABModel.h"

@interface XQABManager : NSObject

+ (instancetype)manager;

/** 获取当前权限 */
+ (XQAuthorizationStatus)getAuStatus;

/** 请求获取权限
 *  granted YES成功
 */
- (void)requestAuStatusWithCompletionHandler:(void(^)(BOOL granted, NSError *error))completionHandler;

/** 获取所有联系人, 如果是用户没有选择过权限, block则是异步回调 */
- (void)getAllContactWithCompletion:(void(^)(NSArray <XQABModel *> *modelArr))completion;

/** 根据传进来的model修改联系人
 *  @param model 联系的model
 *  return YES修改成功
 */
- (BOOL)modificationContactWithModel:(XQABModel *)model;

/** 修改联系人单个属性
 *  @param contactID 联系人ID
 *  @param propertyID 
 *  return YES成功
 */
- (BOOL)modificationContactWithContactID:(int)contactID
                              propertyID:(XQPropertyID)propertyID
                                   value:(id)value;

/** 修改联系人电话
 *  @param contactID 联系人ID
 *  @param phoneLabelArr 标签
 *  @param phoneArr 电话号码
 */
- (BOOL)modificationContactPhoneWithContactID:(int)contactID
                                phoneLabelArr:(NSArray *)phoneLabelArr
                                     phoneArr:(NSArray *)phoneArr;

/** 添加联系人 
 *  @param model 联系人的模板
 *  return YES成功
 */
- (BOOL)addContactWithModel:(XQABModel *)model;

/** 删除联系人
 *  @param contactID 联系人id
 *  return YES成功
 */
- (BOOL)removeContactWithContactID:(int)contactID;


@end














