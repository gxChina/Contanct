//
//  XQABModel.h
//  XQContact
//
//  Created by ladystyle100 on 2017/8/29.
//  Copyright © 2017年 SyKingW. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XQABHeader.h"

@interface XQABModel : NSObject

/** 联系人id */
@property (nonatomic, assign) int contactID;
/** 姓 */
@property (nonatomic, copy) NSString *lastName;
/** 名 */
@property (nonatomic, copy) NSString *firstName;
/** 公司 */
@property (nonatomic, copy) NSString *organization;
/** 头像 */
@property (nonatomic, copy) NSData *imgData;

/** 电话标签数组 */
@property (nonatomic, copy) NSArray *phoneLabelArr;
/** 电话数组, 这两个数组是相对的 */
@property (nonatomic, copy) NSArray *phoneArr;


@end
