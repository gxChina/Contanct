//
//  XQABManager.h
//  AddressBook
//
//  Created by ladystyle100 on 2017/5/6.
//  Copyright © 2017年 WangXQ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol XQABUIManagerDelegate <NSObject>

/** 取消 */
- (void)ABDidCancel;

/** 选中
 *  @prama phoneStr:nil则是选中的没有符合的电话号码
 *  @prama nameStr:@""则是没有名字
 */
- (void)ABDidSelecetWithPhoneStr:(NSString *)phoneStr nameStr:(NSString *)nameStr;

@end

@interface XQABUIManager : NSObject

+ (instancetype)sharedXQABManager;

@property (nonatomic, weak) id <XQABUIManagerDelegate> delegate;

/** 跳转到通讯录 */
- (void)presentABWithVC:(UIViewController *)VC;

@end






