//
//  XQABHeader.h
//  XQContact
//
//  Created by SyKingW on 2017/8/28.
//  Copyright © 2017年 SyKingW. All rights reserved.
//

#ifndef XQABHeader_h
#define XQABHeader_h

#ifdef DEBUG
#define LOG(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#define LOG(...) {}
#endif

#define Weak_Self __weak typeof(self) weakSelf = self;

typedef NS_ENUM(NSUInteger, XQAuthorizationStatus) {
    XQAuthorizationStatusNotDetermined = 0, // 用户未选择权限
    XQAuthorizationStatusRestricted,        // 权限被禁
    XQAuthorizationStatusDenied,            // 用户拒绝
    XQAuthorizationStatusAuthorized         // 允许使用
};

typedef NS_ENUM(int, XQPropertyID) {
    XQPropertyIDUnknow = 0,
    XQPropertyIDLastName,        // 姓
    XQPropertyIDFirstName,       // 名
    XQPropertyIDOrganization,    // 公司
    XQPropertyIDImageData,       // 头像
};

#endif /* XQABHeader_h */

















