//
//  ViewController.m
//  XQContact
//
//  Created by SyKingW on 2017/8/28.
//  Copyright © 2017年 SyKingW. All rights reserved.
//

#import "ViewController.h"
#import "XQABManager.h"
#import "XQABUIManager.h"
#import "AddVC.h"
#import "RemoveVC.h"
#import "ModificationVC.h"

static NSString *reusing_ = @"VCCell";

@interface ViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, copy) NSArray *dataArr;

@property (nonatomic, strong) NSMutableArray <XQABModel *> *modelArr;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dataArr = @[@"获取当前权限", @"申请权限", @"获取所有联系人", @"修改联系人", @"添加联系人", @"删除联系人"];
    
    [self.view addSubview:self.tableView];
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reusing_ forIndexPath:indexPath];
    
    cell.textLabel.text = self.dataArr[indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:{
            NSLog(@"Au = %ld", (long)[XQABManager getAuStatus]);
        }
            break;
            
        case 1:{
            [[XQABManager manager] requestAuStatusWithCompletionHandler:^(BOOL granted, NSError *error) {
                NSLog(@"granted = %d, error = %@", granted, error);
            }];
        }
            break;
            
        case 2:{
            __weak typeof(self) weakSelf = self;
            [[XQABManager manager] getAllContactWithCompletion:^(NSArray<XQABModel *> *modelArr) {
                weakSelf.modelArr = [modelArr mutableCopy];
            }];
            //XQPickerVC *vc = [[XQPickerVC alloc] init];
            //[self presentViewController:vc animated:YES completion:nil];
        }
            break;
            
        case 3:{
            ModificationVC *vc = [ModificationVC new];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        case 4:{
            AddVC *vc = [[AddVC alloc] initWithNibName:NSStringFromClass([AddVC class]) bundle:nil];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        case 5:{
            RemoveVC *vc = [RemoveVC new];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        default:
            break;
    }
    
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark - get

- (UITableView *)tableView {
    
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:reusing_];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.tableFooterView = [UIView new];
        
    }
    return _tableView;
}


@end


















