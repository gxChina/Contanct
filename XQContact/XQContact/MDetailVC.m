//
//  MDetailVC.m
//  XQContact
//
//  Created by ladystyle100 on 2017/8/29.
//  Copyright © 2017年 SyKingW. All rights reserved.
//

#import "MDetailVC.h"
#import "XQABManager.h"

@interface MDetailVC () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *lastName;
@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *organization;
@property (weak, nonatomic) IBOutlet UITextField *phoneLabel;
@property (weak, nonatomic) IBOutlet UITextField *phone;

@property (nonatomic, copy) NSData *imgData;

@end

@implementation MDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.lastName.text = self.model.lastName;
    self.firstName.text = self.model.firstName;
    self.organization.text = self.model.organization;
    self.phoneLabel.text = self.model.phoneLabelArr.firstObject;
    self.phone.text = self.model.phoneArr.firstObject;
    
    self.imgData = self.model.imgData;
}

#pragma mark -- respondsTo

- (IBAction)respondsToSelectIma:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (IBAction)respondsToModification:(id)sender {
    XQABModel *model = [XQABModel new];
    model.contactID = self.model.contactID;
    model.firstName = self.firstName.text;
    model.lastName = self.lastName.text;
    model.organization = self.organization.text;
    model.imgData = self.imgData;
    model.phoneArr = @[self.phone.text];
    model.phoneLabelArr = @[self.phoneLabel.text];
    
    [[XQABManager manager] modificationContactWithModel:model] ?
    [SVProgressHUD showSuccessWithStatus:@"修改成功"]:
    [SVProgressHUD showSuccessWithStatus:@"修改失败"];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *img = info[UIImagePickerControllerOriginalImage];
    self.imgData =  UIImagePNGRepresentation(img);
    if (!self.imgData) {
        self.imgData = UIImageJPEGRepresentation(img, 0.5);
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}


@end













