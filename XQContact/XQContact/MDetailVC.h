//
//  MDetailVC.h
//  XQContact
//
//  Created by ladystyle100 on 2017/8/29.
//  Copyright © 2017年 SyKingW. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XQABModel.h"

@interface MDetailVC : UIViewController

@property (nonatomic, strong) XQABModel *model;

@end
